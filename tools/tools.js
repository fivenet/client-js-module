const fs = require('fs-extra');
const execSync = require('child_process').execSync;

exports.build = () => {
    console.log("Generating Ninja build files")

    createDir("../BUILD")

    let success = false;
    if (process.platform === "win32") {
        success = execCommand("cmake -G \"Ninja\" ..", "../BUILD")
    } else {
        success = execCommand("cmake -G \"Ninja Makefiles\" ..", "../BUILD")
    }

    if (!success) {
        console.log("Could not properly generate build files")
        return
    }

    console.log("Building...")

    execCommand("ninja", "../BUILD")
}

exports.parseArgs = () => {
    const args = {
        platform: "win32",
        config: "debug",
        version: "dev"
    };

    if (process.argv[2] != undefined)
        args.platform = process.argv[2];

    if (process.argv[3] != undefined)
        args.config = process.argv[3];

    if (process.argv[4] != undefined)
        args.version = process.argv[4];

    return args;
}

function createDir(path) {
    try {
        fs.mkdirSync(path);
    } catch (err) {
        if (err.code !== 'EEXIST') throw err;
    }
}

function execCommand(cmd, dir = ".", printOut = true) {
    try {
        if (printOut) {
            execSync(cmd, {
                cwd: dir,
                stdio: [0, 1, 2]
            });
        } else {
            execSync(cmd, {
                cwd: dir,
            });
        }

        return true
    } catch (err) {
        console.error(" -- An error occurred while executing command \n\t'" + cmd + "'\n");
        return false
    }
}