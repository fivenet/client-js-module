const tools = require("./tools.js");

var args = tools.parseArgs();

tools.build(args.platform, args.config, args.version);