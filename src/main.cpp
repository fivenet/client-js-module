#include "cpp-api/API.h"

bool OnResourceLoad(IResource::Task task, IResource* resource) {
	
	return true;
}

bool OnEvent(const CEvent*) {
	
	return true;
}

void OnCommand(const std::vector<std::string>& args) {
	
}

EXPORT bool FivenetMain()
{
	auto& server = IServer::Instance();

	server.RegisterResourceType("client-js", OnResourceLoad);
	server.SubscribeEvent(CEvent::ALL, OnEvent); // crashes here
	server.SubscribeCommand("client-js", OnCommand);
	server.LogInfo("Loaded client-js module");

	return true;
}
